﻿using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _db;
        private IMongoCollection<Employee> _employeeCollection;
        private IMongoCollection<Role> _roleCollection;


        public EfDbInitializer(IMongoDatabase database)
        {
            _db = database;
            _employeeCollection = _db.GetCollection<Employee>("Employee");
            _roleCollection = _db.GetCollection<Role>("Role");
        }

        public void InitializeDb()
        {
            // если коллекции содержат данные, то выходим
            if (_roleCollection.Find(_ => true).Any<Role>() ||
                _employeeCollection.Find(_ => true).Any<Employee>())
                return;

            _roleCollection.InsertManyAsync(FakeDataFactory.Roles);
            _employeeCollection.InsertManyAsync(FakeDataFactory.Employees);
        }
    }
}