﻿using System;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class EfDatabaseFixture: IDisposable
    {
        private readonly EfTestDbInitializer _efTestDbInitializer;
        
        public EfDatabaseFixture()
        {
            var mongoClient = new MongoClient("mongodb://mongoadmin:_Test123@localhost:27018");
            DbContext = mongoClient.GetDatabase("test");

            _efTestDbInitializer = new EfTestDbInitializer(DbContext);
            _efTestDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _efTestDbInitializer.CleanDb();
        }

        public IMongoDatabase DbContext { get; private set; }

    }
}