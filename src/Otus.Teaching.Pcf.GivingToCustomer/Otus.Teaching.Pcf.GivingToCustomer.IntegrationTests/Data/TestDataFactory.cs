﻿using System;
using System.Collections.Generic;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data
{
    public static class TestDataFactory
    {
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static List<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("11b68de1-bcc3-4d32-be0a-6415bf91743d");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<Preference>()
                        {
                            new Preference()
                            {
                                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                                Name = "Дети",
                            },
                            new Preference()
                            {
                                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                                Name = "Театр"
                            }
                        }
                    }
                };

                return customers;
            }
        }
    }
}