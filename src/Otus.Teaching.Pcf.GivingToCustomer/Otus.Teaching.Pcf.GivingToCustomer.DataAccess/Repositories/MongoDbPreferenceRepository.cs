using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbPreferenceRepository
        : IRepository<Preference>
    {
        private readonly IMongoCollection<Preference> _preferenceCollection;
        private string location = "en_US";

        public MongoDbPreferenceRepository(IMongoCollection<Preference> preferenceCollection)
        {
            _preferenceCollection = preferenceCollection;
        }

        public async Task<IEnumerable<Preference>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _preferenceCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _preferenceCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);
        }

        public async Task<IEnumerable<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _preferenceCollection.Find(l => ids.Contains(l.Id),
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Preference> GetFirstWhere(Expression<Func<Preference, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _preferenceCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return result == null ? null : result[0];
        }

        public async Task<IEnumerable<Preference>> GetWhere(Expression<Func<Preference, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _preferenceCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.Name)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(Preference preference)
        {
            await _preferenceCollection.InsertOneAsync(preference);
        }

        public async Task UpdateAsync(Preference preference)
        {
            var filter = Builders<Preference>.Filter.Eq("Id", preference.Id);
            var updatestatement = Builders<Preference>.Update.Set("Name", preference.Name);

            var result = await _preferenceCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update Preference " + preference.Name);
        }

        public async Task DeleteAsync(Preference preference)
        {
            var result = await _preferenceCollection.DeleteOneAsync<Preference>( l => l.Id == preference.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete Preference " + preference.Name);
        }

    }
}