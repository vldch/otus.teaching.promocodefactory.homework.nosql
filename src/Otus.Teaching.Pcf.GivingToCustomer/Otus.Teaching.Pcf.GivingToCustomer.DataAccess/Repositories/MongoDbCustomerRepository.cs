using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;


namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbCustomerRepository
        : IRepository<Customer>
    {
        private readonly IMongoCollection<Customer> _customerCollection;
        private string location = "en_US";

        public MongoDbCustomerRepository(IMongoCollection<Customer> customerCollection)
        {
            _customerCollection = customerCollection;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _customerCollection.Find(_ => true,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.LastName)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var customer = await _customerCollection.Find(l => l.Id == id,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    }).FirstOrDefaultAsync(cancellation.Token);

            return customer;
        }

        public async Task<IEnumerable<Customer>> GetRangeByIdsAsync(List<Guid> ids)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _customerCollection.Find(l => ids.Contains(l.Id),
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.LastName)
                .ToListAsync(cancellation.Token);
        }

        public async Task<Customer> GetFirstWhere(Expression<Func<Customer, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            var result = await _customerCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .ToListAsync(cancellation.Token);

            return result == null ? null : result[0];
        }

        public async Task<IEnumerable<Customer>> GetWhere(Expression<Func<Customer, bool>> predicate)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource(10000);
            return await _customerCollection.Find(predicate,
                    new FindOptions
                    {
                        Collation = new Collation(location)
                    })
                .SortBy(l => l.LastName)
                .ToListAsync(cancellation.Token);
        }

        public async Task AddAsync(Customer entity)
        {
            await _customerCollection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Customer entity)
        {
            var filter = Builders<Customer>.Filter.Eq("Id", entity.Id);

            var updatestatement = Builders<Customer>.Update.Set("FirstName", entity.FirstName);
            updatestatement = updatestatement.Set("LastName", entity.LastName);
            updatestatement = updatestatement.Set("Email", entity.Email);
            updatestatement = updatestatement.Set("Preferences", entity.Preferences);
            updatestatement = updatestatement.Set("PromoCodeIds", entity.PromoCodeIds);

            var result = await _customerCollection.UpdateOneAsync(filter, updatestatement);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to update Customer " + entity.FullName);
        }

        public async Task DeleteAsync(Customer entity)
        {
            var result = await _customerCollection.DeleteOneAsync<Customer>( l => l.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception("Unable to delete Customer " + entity.FullName);
        }

    }
}